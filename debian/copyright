Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Wiki application
Upstream-Contact: Horde <horde@lists.horde.org>
                  Horde development <dev@lists.horde.org>
Source: http://pear.horde.org/

Files: *
Copyright: 2003-2017, Horde LLC (http://www.horde.org)
           2004-2017, Horde LLC (http://www.horde.org)
           2008-2017, Horde LLC (http://www.horde.org)
           2009-2017, Horde LLC (http://www.horde.org)
           2010-2017, Horde LLC (http://www.horde.org)
           2011-2017, Horde LLC (http://www.horde.org)
License: GPL-2

Files: wicked-*/lib/Text_Wiki/*
Copyright: Paul M. Jones <pmjones@php.net>
           2005, Bertrand Gugger <bertrand@toggg.com>
           Justin Patrin <papercrane@reversefold.com>
           Michele Tomaiuolo <tomamic@yahoo.it>
           Firman Wandayandi <firman@php.net>
           Manuel Holtgrewe <purestorm at ggnore dot net>
           Brian J. Sipos <bjs5075@rit.edu>
           Moritz Venn <ritzmo@php.net>
License: LGPL-2.1
Comment:
 Listed authors and copyright holders both as copyright holders
 as the license header field usage is not consistent and several
 of the listed authors are probably also copyright holders.

Files: debian/*
Copyright: 2012-2019, Mathieu Parent <math.parent@gmail.com>
           2020, Mike Gabriel <mike.gabriel@das-netzwerkteam.de>
License: LGPL-2.1+ or GPL-2

License: GPL-2
 This program is free software; you can redistribute it
 and/or modify it under the terms of the GNU General Public
 License as published by the Free Software Foundation.
 .
 This program is distributed in the hope that it will be
 useful, but WITHOUT ANY WARRANTY; without even the implied
 warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 PURPOSE.  See the GNU General Public License for more
 details.
 .
 You should have received a copy of the GNU General Public
 License along with this package; if not, write to the Free
 Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA  02110-1301 USA
 .
 On Debian systems, the full text of the GNU General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: LGPL-2.1
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation;
 version 2.1 of the License.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.

License: LGPL-2.1+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 .
 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 .
 On Debian systems, the full text of the GNU Lesser General Public
 License version 2.1 can be found in the file
 `/usr/share/common-licenses/LGPL-2.1'.
