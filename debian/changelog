php-horde-wicked (2.0.8-9) unstable; urgency=medium

  [ Mike Gabriel ]
  * d/control: Bump Standards-Version: to 4.6.2. No changes needed.
  * d/watch: Switch to format version 4.
  * debian/patches: Add 1012_php8.2.patch. Fix errors when running unit tests
    on PHP 8.2. (Closes: #1003784).

  [ Anton Gladky ]
  * d/salsa-ci.yml: use aptly, simplify.

 -- Mike Gabriel <sunweaver@debian.org>  Fri, 10 Feb 2023 08:28:39 +0100

php-horde-wicked (2.0.8-8) unstable; urgency=medium

  * d/patches: Add 1010_phpunit-8.x+9.x.patch. Fix tests with PHPUnit 8.x/9.x.
  * d/t/control: Require php-horde-test (>= 2.6.4+debian0-6~).
  * d/patches: Add 1001_use-square-braces-for-accessing-string-offsets.patch.
    Don't access string offsets via curly braces.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 03 Oct 2020 13:31:20 +0200

php-horde-wicked (2.0.8-7) unstable; urgency=medium

  * d/control: Enforce versioned D on php-horde (>= 5.2.23+debian0-2~).
  * d/rules: Move theme files to /etc/horde/.
  * debian/maintscript: Turn /usr/share/horde/wicked/themes/ into symlink.

 -- Mike Gabriel <sunweaver@debian.org>  Sun, 05 Jul 2020 00:16:22 +0200

php-horde-wicked (2.0.8-6) unstable; urgency=medium

  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.
  * d/salsa-ci.yml: Add file with salsa-ci.yml and pipeline-jobs.yml calls.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 15:41:16 +0200

php-horde-wicked (2.0.8-5) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959362).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/copyright: Update copyright attributions.
  * d/upstream/metadata: Add file. Comply with DEP-12.
  * d/tests/control: Stop using deprecated needs-recommends restriction.
  * d/copyright: Update copyright attributions.
  * d/rules: Clean-up after dh_install.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 21 May 2020 10:17:00 +0200

php-horde-wicked (2.0.8-4) unstable; urgency=medium

  * Bump debhelper from old 11 to 12.
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 20:27:24 +0200

php-horde-wicked (2.0.8-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 May 2018 22:40:50 +0200

php-horde-wicked (2.0.8-2) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Fri, 06 Apr 2018 20:34:01 +0200

php-horde-wicked (2.0.8-1) unstable; urgency=medium

  * New upstream version 2.0.8

 -- Mathieu Parent <sathieu@debian.org>  Wed, 27 Sep 2017 23:26:19 +0200

php-horde-wicked (2.0.7-1) unstable; urgency=medium

  * New upstream version 2.0.7

 -- Mathieu Parent <sathieu@debian.org>  Mon, 19 Dec 2016 09:33:45 +0100

php-horde-wicked (2.0.6-1) unstable; urgency=medium

  * New upstream version 2.0.6

 -- Mathieu Parent <sathieu@debian.org>  Sun, 03 Jul 2016 07:34:39 +0200

php-horde-wicked (2.0.5-2) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Wed, 08 Jun 2016 20:51:02 +0200

php-horde-wicked (2.0.5-1) unstable; urgency=medium

  * New upstream version 2.0.5

 -- Mathieu Parent <sathieu@debian.org>  Sat, 26 Mar 2016 14:49:16 +0100

php-horde-wicked (2.0.4-3) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition
  * Replace php5-* by php-* in d/tests/control

 -- Mathieu Parent <sathieu@debian.org>  Mon, 14 Mar 2016 10:15:48 +0100

php-horde-wicked (2.0.4-2) unstable; urgency=medium

  * Upgaded to debhelper compat 9

 -- Mathieu Parent <sathieu@debian.org>  Sat, 24 Oct 2015 14:18:26 +0200

php-horde-wicked (2.0.4-1) unstable; urgency=medium

  * Remove XS-Testsuite header in d/control
  * Update gbp.conf
  * New upstream version 2.0.4
  * Use override_dh_link instead of dh deprecated --until/--after

 -- Mathieu Parent <sathieu@debian.org>  Tue, 11 Aug 2015 08:51:03 +0200

php-horde-wicked (2.0.3-1) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change
  * New upstream version 2.0.3

 -- Mathieu Parent <sathieu@debian.org>  Tue, 05 May 2015 08:47:43 +0200

php-horde-wicked (2.0.1-4) unstable; urgency=medium

  * Fixed DEP-8 tests, by removing "set -x"

 -- Mathieu Parent <sathieu@debian.org>  Sat, 11 Oct 2014 16:09:24 +0200

php-horde-wicked (2.0.1-3) unstable; urgency=medium

  * Fixed DEP-8 tests

 -- Mathieu Parent <sathieu@debian.org>  Sat, 13 Sep 2014 16:44:55 +0200

php-horde-wicked (2.0.1-2) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb
  * Add dep-8 (automatic as-installed package testing)

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Aug 2014 22:29:52 +0200

php-horde-wicked (2.0.1-1) unstable; urgency=low

  * New upstream version 2.0.1

 -- Mathieu Parent <sathieu@debian.org>  Tue, 29 Oct 2013 21:27:10 +0100

php-horde-wicked (2.0.0-1) unstable; urgency=low

  * New upstream version 2.0.0

 -- Mathieu Parent <sathieu@debian.org>  Tue, 17 Sep 2013 22:57:28 +0200

php-horde-wicked (2.0.0~beta1-1) unstable; urgency=low

  * New upstream version 2.0.0~beta1
  * H5 version (Closes: #695900)

 -- Mathieu Parent <sathieu@debian.org>  Wed, 12 Jun 2013 20:51:13 +0200

php-horde-wicked (1.0.2-4) unstable; urgency=low

  * Use pristine-tar

 -- Mathieu Parent <sathieu@debian.org>  Thu, 06 Jun 2013 09:19:37 +0200

php-horde-wicked (1.0.2-3) unstable; urgency=low

  * Add a description of Horde in long description
  * Updated Standards-Version to 3.9.4, no changes
  * Replace horde4 by PEAR in git reporitory path
  * Fix Horde Homepage
  * Remove debian/pearrc, not needed with latest php-horde-role

 -- Mathieu Parent <sathieu@debian.org>  Wed, 09 Jan 2013 20:30:20 +0100

php-horde-wicked (1.0.2-2) unstable; urgency=low

  * php-horde-role needed at buildtime

 -- Mathieu Parent <sathieu@debian.org>  Sun, 09 Dec 2012 16:19:52 +0100

php-horde-wicked (1.0.2-1) unstable; urgency=low

  * wicked package
  * Initial packaging (Closes: #695419)

 -- Mathieu Parent <sathieu@debian.org>  Sat, 08 Dec 2012 22:48:41 +0100
